import java.util.ArrayList;
import java.util.Scanner;



public class Reversi {
	
	private static final int[] offset_row = {-1, -1, -1, 0, 0, 1, 1, 1}; //Used in order to check all 8 directions out from a placed piece (row-wise)
	private static final int[] offset_col = {-1, 0, 1, -1, 1, -1, 0, 1}; //Used in order to check all 8 directions out from a placed piece (col-wise)
	/*
		. . .		1 2 3		(-1,-1) (-1,0) (-1,1)
		. X .    ==> 	4 X 5	==>	(0,-1)    X    (0,1)	 
		. . .		6 7 8		(1,-1)  (1,0)  (1,1)
		
		Taken from: https://github.com/luugiathuy/ReversiGame/blob/master/src/com/luugiathuy/games/reversi/Reversi.java
	*/

	private static final int BLACK = 2;
	private static final int WHITE = 1;
	private static final int EMPTY = 0;
	private static final int NROWS = 8;
	private static final int NCOLS = 8;
	private static final String FORFEIT = "I0";

	private static class Timer {
		private long timeLimit;					//How many milliseconds can the player(s) take
		private boolean active;					//Is the clock ticking?
		private long timeActivated = 0;				//At what time did clock start ticking?
		private ArrayList<Long> book = new ArrayList<Long>();	//Keep track of time/move (buggy! See comments below)
		private boolean humanTimer = false;			//Does the timer apply to human player ALSO (Note: AI is always subject to timer)

		//Constructor
		public Timer(long t){
			timeLimit=Math.abs(t*1000);			//Abs-value since user could have inputted t<0 (meaning human not subject to timing constraints)
			if (t>0)
				humanTimer = true;
			active = false;
		}
		public long getLimit(){
			return timeLimit;
		}
		public boolean stressHuman(){
			return humanTimer;
		}
		public void setLimit(long n){
			timeLimit = n;
		}
		public void activateTimer(){
			active = true;
			timeActivated = now();
		}
		public long deActivateTimer(){
			active = false;
			long diff = getTimeDiff();

			book.add(diff); /*The idea is that everytime the timer is deactivated a 
					move is over and as such can be added to a log (book) for future
					reference. This idea fails completely if the timer doesn't
					apply to the human or if, at any time, there is a forfeited move. */
			return diff;
		}
		public long getTimeDiff(){
			return now() - timeActivated;
		}

		//Gives the current system time in milliseconds
		public long now(){
			return System.currentTimeMillis();
		}

		public boolean enoughTime(){
			if (active==false){
				return true;
			}
			if (getTimeDiff() >= timeLimit){
				return false;
			}
			return true;
		}
		public void printBook(boolean ai_start){
			int ai_index = ai_start?0:1;
			long sum_ai = 0;
			long sum_human = 0;
			int playCount = 1;
			for (int i=0; i<book.size(); i++){
				String text = "";
				long currT = book.get(i);
				if (i%2==ai_index){
					sum_ai += currT;
					text ="(AI    #";
					playCount++;
				}else{
					sum_human += currT;
					text ="(HUMAN #";
				}
				/*PLEASE NOTE! This will write wrong results as soon as a round is forfeited!! 
				This print assumes that the rounds are ALWAYS alernating between player and AI.
				This might not be the case. Please be advised that it might say that a Human made
				a move which the AI actually did and vice versa!

				The only realible metric is what comes after the second tab, i.e (i+1)*/
				text += playCount-(i%2)+")";
				System.out.println("\t"+text+"\tmove #"+(i+1)+" took "+currT+"milliseconds to make.");
			}
			System.out.println("Total reasoning time for the AI = "+sum_ai+" milliseconds ~= "+sum_ai/1000+"seconds.");
			System.out.println("Total reasoning time for the human = "+sum_human+" milliseconds ~= "+sum_human/1000+"seconds.");
		}
	}

	public static void main(String[] args) {
		boolean compStart = false; //Default, human (white) starts
		int depth = 6;
		long maxTime = -12;
		boolean printTime = false;
		if (args.length > 0 && Integer.parseInt(args[0])<0){
			//If first argument is negative, computer starts play
			compStart = true;
		}
		if (args.length > 1){
			//If second argument provided it will indicate depth of search tree
			depth = Integer.parseInt(args[1]);
		}
		if (args.length > 2){
			//If third argument provided it will indicate max time to think in seconds
			maxTime = Long.parseLong(args[2]);
		}
		if (args.length > 3){
			//If foruth argument provided it will print out time taken for each move
			printTime = true;
		}
		Timer theTimer = new Timer(maxTime);


		int gameboard[][] = new int[NROWS][NCOLS];
		gameboard[3][3] = WHITE; //4d
		gameboard[4][4] = WHITE; //5e
		gameboard[4][3] = BLACK; //5d
		gameboard[3][4] = BLACK; //4e

		String starter = compStart?"Yes":"No";
		
		System.out.println("\n\nCreating game board of size "+NCOLS+"x"+NROWS);
		System.out.println("Computer start? "+starter+"!");
		System.out.println("Depth of search: "+depth);
		String timeText = maxTime<0?" (Only applies to AI)":" (For both players)";
		System.out.println("Max seconds: "+Math.abs(maxTime)+timeText);

		//computer is black.
		System.out.println("GAME START!\nYou are WHITE, computer is BLACK.\n");
		play(gameboard, compStart, false, depth, theTimer);
		int ai_score = countScore(gameboard);

		int black = 0;
		int white = 0;
		int empty = 0;
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				if(gameboard[i][j] == BLACK){
					black++;
				}else if(gameboard[i][j] == WHITE){
					white++;
				}
				else{
					empty++;
				}
			}
		}	

		System.out.println("Board contains "+black+" black pieces, "+white+" white pieces and "+empty+" empty places");
		if(ai_score>0){
			System.out.println("Computer/AI wins with "+ai_score+" more pieces than human!");
		}
		else if (ai_score == 0) {
			System.out.println("It's a tie!");	
		}
		else{
			System.out.println("Human wins with "+Math.abs(ai_score)+" more pieces than AI!");
		}

		if (printTime){
			theTimer.printBook(compStart);	
		}
		
		
	}

	private static void play(int[][] gameboard, boolean isCompTurn, boolean previousPlayerNoMoves, int searchDepth, Timer theTimer) {
				
		ArrayList<String> moves = possibleMoves(gameboard, isCompTurn);
		drawBoard(gameboard);
		
		boolean noMoves = moves.isEmpty();
		if (noMoves && previousPlayerNoMoves){
			System.out.println("No players can make any moves - the game is over!");
			return;
		}
		String colour = isCompTurn ? "black" : "white";
		System.out.println("It is " + colour + "'s turn to move. Possible moves: " + moves.toString());
		
		//will be set later to the move by computer or human
		int row = 0;
		int col = 0;
		boolean forfeit = false;
		boolean timerAppliesToHuman = theTimer.stressHuman();
		long playTime = theTimer.now();
		if(!noMoves){
			String chosenMove = "";
			if(isCompTurn){
				//find best move for comp
				theTimer.activateTimer();
				int maxScore = Integer.MIN_VALUE;
				String bestMove = moves.get(0)	; //Default choice is first available move
				if (moves.size() > 1){
					//If the computer can only choose one move there is no point in running simulations
					for(String move : moves){
						int gameBoardCopy[][] = new int[8][8];
						gameBoardCopy = copyGameBoard(gameboard);
						int score = simulateMove(gameBoardCopy, move, searchDepth, true,theTimer);					
						if(score > maxScore){
							maxScore = score;
							bestMove = move;
						}
					}
				}
				playTime = theTimer.deActivateTimer();
				System.out.println("Computer chose move: " + bestMove + " after "+playTime+" milliseconds.");
				row = bestMove.charAt(0) - 49;
				col = bestMove.charAt(1) - 97;
			}else{
				boolean legalMove = false;
				if (timerAppliesToHuman){
					theTimer.activateTimer();	
				}
				@SuppressWarnings("resource")
				Scanner in = new Scanner(System.in);
				while(!legalMove && theTimer.enoughTime()){
					//check if the move was legal
					String move = in.next();
					if (move.charAt(0) == '!'){
						System.out.println("Cheater! Timer is reset.");
						theTimer.activateTimer();
					}
					for(int i = 0; i < moves.size(); i++){
						if(move.equals(moves.get(i))){
							row = move.charAt(0) - 49;
							col = move.charAt(1) - 97;					
							legalMove = true;
							break;
						}
					}			
					if(!legalMove){
						//entered move was not legal
						System.out.println("Must enter a possible move!");
					}else{
						chosenMove = move;
					}
				}
				
				if (!legalMove || !theTimer.enoughTime()){ //This will fire if the time runs out
					forfeit = true;
				}
				if (timerAppliesToHuman){
					playTime = theTimer.deActivateTimer();	
				}else{
					playTime = theTimer.now()-playTime;
				}
				
				
			}
			if (!forfeit){
				if(!isCompTurn)
					System.out.println("You chose move: "+chosenMove+ " after "+playTime+" milliseconds.");
				makeMove(gameboard, row, col, isCompTurn);	
			}else{
				System.out.println("\nPlay took to long time! No move for you! \nThe timer limit is "+theTimer.getLimit()+"[ms] and you took "+theTimer.getTimeDiff()+"[ms] to finish your move!");
			}
			
		}else{
			System.out.println("\nCurrent player cannot move - automatically forfeit!");
		}		
		isCompTurn = !isCompTurn;
		play(gameboard, isCompTurn, noMoves,searchDepth,theTimer);
	}

	private static int simulateMove(int[][] gameboard, String move, int counter,
			boolean isComp, Timer theTimer) {	
		
		int row = move.charAt(0) - 49;
		int col = move.charAt(1) - 97;
		//If a forfeit-move has been passed do as usual except no modification
		// of board.. in practice we "give" the turn to the other player
		if (move != FORFEIT){
			makeMove(gameboard, row, col, isComp); 	
		}
		
		if (counter == 0 || !theTimer.enoughTime() ){
			return countScore(gameboard);
		}
		counter=counter-1;
		


		int maxScore = Integer.MIN_VALUE;
		int minScore = Integer.MAX_VALUE;

		ArrayList<String> counterMoves = possibleMoves(gameboard, !isComp);
		if (counterMoves.isEmpty()){
			//If the opposing player has no countermoves we "inject" a forfeit (which is what would happen in reality)
			counterMoves.add(FORFEIT);
		}
		int gameBoardOriginal[][] = new int[NROWS][NCOLS];
		gameBoardOriginal = copyGameBoard(gameboard);
		for(String counterMove : counterMoves){			
			int score = simulateMove(gameboard, counterMove, counter,!isComp, theTimer);
			gameboard = copyGameBoard(gameBoardOriginal);
			
			if (isComp){
				//Set max score
				if(score > maxScore){
					maxScore = score;
				}	
			}
			else{
				//Set min score
				if (score < minScore){
					minScore = score;
				}
			}
		}
		
		return isComp ? maxScore:minScore;
		//Human is minimizer, return min
		//Computer is maximizer, return max
	}

	private static void makeMove(int[][] gameboard, int affectedRow, int affectedCol, boolean isCompTurn) {		
		int samePiece = isCompTurn ? BLACK : WHITE;
		gameboard[affectedRow][affectedCol] = samePiece;
		for(int k = 0; k < 8; k++){
			int row = affectedRow + offset_row[k];
			int col = affectedCol + offset_col[k];
			//if first next piece in this direction is same, don't bother check the rest of that direction.
			if(row >= 0 && row < 8 && col >= 0 && col < 8 && gameboard[row][col] == samePiece){
				continue;
			}
			boolean hasOppPieceBetween = false;
			while(row >= 0 && row < 8 && col >= 0 && col < 8){
				if(gameboard[row][col] == EMPTY){
					break;
				}
				if(gameboard[row][col] != samePiece){
					hasOppPieceBetween = true;
				}
				if(gameboard[row][col] == samePiece && hasOppPieceBetween){
					int curRow = affectedRow + offset_row[k];
					int curCol = affectedCol + offset_col[k];
					while(curRow != row || curCol != col){
						if(gameboard[curRow][curCol] != samePiece){
							gameboard[curRow][curCol] = samePiece;
						}
						curRow += offset_row[k];
						curCol += offset_col[k];
					}
				}
				row += offset_row[k];
				col += offset_col[k];
			}
		}
	}

	private static int[][] copyGameBoard(int[][] gameboard) {
		int[][] copy = new int[8][8];
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				copy[i][j] = gameboard[i][j];
			}
		}
		return copy;
	}

	private static void drawBoard(int[][] gameboard) {
		System.out.println("\n\t  a b c d e f g h");		
		for(int i = 0; i < 8; i++){
			char row = (char) (i + 49);
			System.out.print("\t"+row + " ");
			for(int j = 0; j < 8; j++){
				String print = "";
				if(gameboard[i][j] == EMPTY){
					print = "·";
				}else{
					print = gameboard[i][j] == BLACK ? "B" : "W";
				}
				System.out.print(print + " ");
			}
			System.out.println("");
		}
		System.out.println("");		
	}

	private static int countScore(int[][] gameboard) {
		//The score is from the AI's perspective. A positive number indicates how many MORE pieces that computer has over human. A negative number means a lead for human
		int black = 0;
		int white = 0;
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				if(gameboard[i][j] == BLACK){
					black++;
				}else if(gameboard[i][j] == WHITE){
					white++;
				}
			}
		}		
		return black - white;
	}

	private static ArrayList<String> possibleMoves(int[][] gameboard, boolean isCompTurn) {
		ArrayList<String> moves = new ArrayList<String>();
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				String move = "";
				if(isValidMove(gameboard, i, j, isCompTurn)){
					move += i + 1;
					char col = (char) (j + 97);
					move += col;
					moves.add(move);
				}
			}
		}
		return moves;
	}

	private static boolean isValidMove(int[][] gameboard, int i, int j, boolean isCompTurn) {
			// This check should be expanded with additional clause to see if the row/column/diagonal has the current players color in it
			if(gameboard[i][j] != 0){
				return false;
			}

			int samePiece = isCompTurn ? BLACK : WHITE;
			for(int k = 0; k < 8; k++){
				int row = i + offset_row[k];
				int col = j + offset_col[k];
				//if first next piece in this direction is same, don't bother check the rest of that direction.
				if(row >= 0 && row < 8 && col >= 0 && col < 8 && gameboard[row][col] == samePiece){
					continue;
				}
				boolean hasOppPieceBetween = false;
				while(row >= 0 && row < 8 && col >= 0 && col < 8){
					if(gameboard[row][col] == EMPTY){
						break;
					}
					if(gameboard[row][col] != samePiece){
						hasOppPieceBetween = true;
					}
					if(gameboard[row][col] == samePiece && hasOppPieceBetween){
						return true;
					}
					row += offset_row[k];
					col += offset_col[k];
				}
			}
			return false;
	}
}
